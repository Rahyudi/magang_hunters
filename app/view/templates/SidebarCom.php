<!-- SideBar Section -->
<div id="layoutSidenav" class="mt-1">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <div class="sb-sidenav-menu-heading">Core</div>
                    <a class="nav-link" href="<?= BASEURLJOBS; ?>/company/index/<?= $_SESSION['UserID'] ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-home"></i></div>
                        Home
                    </a>
                    <a class="nav-link" href="<?= BASEURLJOBS; ?>/company/joblist/<?= $_SESSION['UserID'] ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-envelope"></i></div>
                        Joblist
                    </a>
                    <a class="nav-link" href="<?= BASEURLJOBS; ?>/company/profile/<?= $_SESSION['UserID'] ?>">
                        <div class="sb-nav-link-icon"><i class="fas fa-gear"></i></div>
                        Profile
                    </a>
        </nav>
    </div>